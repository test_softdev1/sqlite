/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ananchai.sqliteproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

/**
 *
 * @author Lenovo
 */
public class InsertUser {
   public static void main( String args[] ) {
      Connection conn = null;
      Statement stmt = null;
      
      try {
         Class.forName("org.sqlite.JDBC");
         conn = DriverManager.getConnection("jdbc:sqlite:user.db");
         conn.setAutoCommit(false);
         System.out.println("Opened database successfully");

         stmt = conn.createStatement();
         String sql = "INSERT INTO user (id,username,password) " +
                        "VALUES (4, 'art', 'password');"; 
         stmt.executeUpdate(sql);
         
         conn.commit();
         stmt.close();
         conn.close();
      } catch ( Exception e ) {
         System.err.println( e.getClass().getName() + ": " + e.getMessage() );
         System.exit(0);
      }
      System.out.println("Records created successfully");
   }
}
