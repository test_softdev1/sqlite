/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ananchai.sqliteproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author Lenovo
 */
public class DeleteUser {
    public static void main(String args[]) {
        Connection conn = null;
        Statement stmt = null;

        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:user.db");
            conn.setAutoCommit(false);    
            stmt = conn.createStatement();
            
            String sql = "DELETE from user where ID=2;";
            stmt.executeUpdate(sql);
            conn.commit();
            //select
            ResultSet rs = stmt.executeQuery("SELECT * FROM user;");

            while (rs.next()) {
                int id = rs.getInt("id");
                String username = rs.getString("username");        
                String password = rs.getString("password");


                System.out.println("id = " + id);
                System.out.println("username = " + username);
                System.out.println("password = " + password);
                System.out.println();
            }

            rs.close();
            stmt.close();
            conn.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        System.out.println("Records created successfully");
    }
}
